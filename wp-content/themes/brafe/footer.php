<footer>
        <div id="footer" class="container">
            <div>
                <?php the_field("direitos_autorais") ?><br> <?php the_field("endereco") ?>
                <!-- Não consegui colocar as alterações do footer para todas as páginas
                Que tipo(s) de função posso usar aqui, seria algo como foi no header? -->
            </div> 
            <div>
                <img class="img_brafe" src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="logo Brafé">
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>