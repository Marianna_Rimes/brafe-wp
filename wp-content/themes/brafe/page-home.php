<?php 
    // Template Name: Home Page
?>

<?php get_header(); ?>
    <main>
        <div id="main_div1">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-intro.jpg" alt="Canecas de café">
            <div id="main_div2">
                <h1> <?php the_field("titulo_chamativo") ?></h1> 
                <p id="main_p"><?php the_field("subtitulo_chamativo") ?></p>
            </div>
        </div>
    </main>
    
    <section>
        <h2>Uma Mistura de</h2>
        <div class="flex_center">
            <div class="sec1_div1">
                <figure>
                    <img src="<?php the_field("imagem_esquerda") ?>" alt="xicara com coração">
                </figure>
                <div class="sec1_div2"> <?php the_field("legenda_esquerda") ?></div>
            </div>
            <div class="sec1_div1">
                <img src="<?php the_field("imagem_direita") ?>" alt="xicara de café">
                <div class="sec1_div2"><?php the_field("legenda_direita")?></div>
            </div>
        </div>
        <p id="sec1_p"> <?php the_field ("texto_centralizado") ?> </p>
    </section>
    
    <section class="section">
        <div class="flex_center container">
            <div class="sec2_div1">
                <div class="sec2_div2 cor_paulista">
                    <button class="sec2_button cor_paulista" ></button>
                </div>
                <h3><?php the_field("titulo_1") ?></h3>
                <p class="sec2_p"> <?php the_field("texto_1") ?></p>
            </div>                
            <div class="sec2_div1">
                <div class="sec2_div2 cor_carioca">
                    <button class="sec2_button cor_carioca"></button>
                </div>
                <h3><?php the_field("titulo_2") ?></h3>
                <p class="sec2_p"> <?php the_field("texto_2") ?></p>
            </div>
            <div class="sec2_div1">
                <div class="sec2_div2 cor_mineiro">
                    <button class="sec2_button cor_mineiro"></button>
                </div>                
                <h3><?php the_field("titulo_3") ?></h3>
                <p class="sec2_p"> <?php the_field("texto_3") ?></p>
            </div>
        </div>
        <button class="button">SAIBA MAIS</button>
    </section>

    <section>
        <div class="flex_center container">
            <div class="margin">
                <img src="<?php the_field("primeira_imagem") ?>" alt="loja botafogo">
            </div>
            <div class="sec3_div">
                <h3><?php the_field ("primeiro_titulo") ?></h3>
                <p class="sec3_p"><?php the_field("primeira_descricao")?> </p>
                <button class="button sec3_button">VER MAPA</button>
            </div>
        </div>
        <div class="flex_center container">
            <div class="margin">
                <img src="<?php the_field("segunda_imagem") ?>" alt="loja iguatemi">
            </div>
            <div class="sec3_div">
                <h3> <?php the_field("segundo_titulo") ?> </h3>
                <p class="sec3_p"> <?php the_field("segunda_descricao") ?>   </p>
                <button class="button sec3_button">VER MAPA</button>
            </div>
        </div>
        <div class="flex_center container">
            <div class="margin">
                <img src="<?php the_field("terceira_imagem") ?>" alt="loja mineirão">
            </div>
            <div class="sec3_div">
                <h3> <?php the_field("terceiro_titulo") ?> </h3>
                <p class="sec3_p"> <?php the_field("terceira_descricao") ?>  </p>
                <button class="button sec3_button">VER MAPA</button>
            </div>
        </div>
    </section>

    <section class="section">
        <div id="sec4_div" class="container">
            <div>
                <h3>Assine Nossa Newsletter</h3>
                <h4>promoções e eventos mensais</h4>
            </div>
            <form>
                <input type="email" name="e-mail" placeholder="Digite seu e-mail"  id="">
                <input type="submit" value="ENVIAR">
            </form>
        </div>
    </section>

<?php get_footer(); ?>