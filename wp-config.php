<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gmtdLVxPmNuUoqsnWklMJtK08EY1KwfI6xH8lo0FwFQlVraUKldK9iDoMJx6OzD6Z9ngjJf5ENVPRBpUs7QwAg==');
define('SECURE_AUTH_KEY',  'tC9z9ohyOikyjdUe5DUKhJ68CaGw7VBeskgnILpHeQYWiAWBrOibTfcM/Rjr9rPVfcfQsKeOa1ll/WHPth6M6Q==');
define('LOGGED_IN_KEY',    'xJHFqmIibZma1fg7ZPW+V5dlasSjUaR9cqze9ljKoIDNWEZmGmtFLtthTJqHn2WEvEe7PURWk+k4NaFDQ+2mAQ==');
define('NONCE_KEY',        'yvcBh5Xj1KwbSDEcHpDJsls7or+cdKoBShlKtfgd92HlHZuUru8zNNF4m3rNbLOemYhnLdAmbHqh4VcXJq+06A==');
define('AUTH_SALT',        'w7ZzETZ+7YXiJopSNm5agJ3bVD6BtDILZ0O/CagU5ro50+5RvPTICw0kLCtg26h1wygr0CArCYRIyiARaf8Ncw==');
define('SECURE_AUTH_SALT', 'jwY06JGqCChiP1287/HcoYZ49oRvR2D+43DbGLk/eEUwxBuoJRO9OzoeyICZEbsux89OnN6ByYcqalTrAwfgHQ==');
define('LOGGED_IN_SALT',   'zgJaArLnInwuId9X4tViMHukAZAUdQE1RrXsN8XQVm6xAtYTNvHBPCfoJSWUU1fRn0FdS+0zx5Ad2u+qtTfSJg==');
define('NONCE_SALT',       'Lg83hdZHJschKWUs8C+vCzNuzAm3USV2A7s+OE9SOPU3iDQu8cq9mtxxeRRx5m1DwBme9tak6OQEa5PWc40XZw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
